tartujs
=======

Welcome to tartujs. What's that?, you ask. Well,

> I'm an idealist. I don't know where I'm going, but I'm on my way.<br/>
> -- Carl Sandburg

## Documentation

See [documentation](doc/README.md)

## Frontend Development

Under this directory, an *assets* directory is used to store frontend bower components. This directory doesn't need to be exist, it will be created if bower find no *assets* directory. This should be installed using [Bower](http://bower.io). The [bower.json file](bower.json) is intended as a note for package dependencies. Go to parent directory of this dir where *bower.json* reside and then `bower install` to install them. All of js package are not included here for simplicity reasons. Once I see the need to put them all together, then I will put them all here. In the meantime, install them by yourself.

My $HOME/.bowerrc configuration file consists of these lines:

```
{
	"directory" : "assets"
}
```

That is why all of js clients will be installed inside *assets* dir. Check yours and make your own adjustment.

## License

Copyright © 2014 Bambang Purnomosidi D. P.

Distributed under the Eclipse Public License 1.0
